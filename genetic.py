import random
import time
from typing import List
import numpy as np

# from util import util as Util, state as State, grupo as Grupo
from util.state import State
from util.grupo import Grupo
from util.util import *
from typing import List

def convergent(states : List[State]):
    conv = False
    if states != None:
        base = states[0]
        i = 0
        while i < len(states):
            if not base.igualdade(states[i]):
                return False
            i += 1
        return True

def gerar_populacao(k : int, pontos : List, tam_populacao : int) -> List[State]:
    i = 0
    populacao = [None] * tam_populacao
    while i < tam_populacao:
        state : State = State(k, pontos) # cria um novo estado de forma aleatoria
        state.init_state()
        state.calc_sse_total()
        populacao[i] = state
        i += 1
    return populacao

def crossover_aux(mae : State, pai : State):
    r = random.randint(0, len(mae.grupos)-1)
    return mae.grupos[:r] + pai.grupos[r:]

def crossover(mae : State, pai : State):
    filho : State = State(mae.k, mae.points)
    filho.grupos = crossover_aux(mae, pai)
    filho.calc_sse_total()
    
    filha : State = State(mae.k, mae.points)
    filha.grupos = crossover_aux(pai, mae)
    filha.calc_sse_total()

    return filho, filha

def menos_apto(populacao : List[State]) -> State:
    aptidao : float = populacao[0].sse_total
    menos_apto = None 
    for state in populacao:
        aux_aptidao = state.sse_total
        if aux_aptidao >= aptidao:
            aptidao = aux_aptidao
            menos_apto = state
    return menos_apto



def mutacao(individuo : State):
    individuo.pertubarcao_estado()

def mutacao_step(populacao : List[State], mutation_ratio : float) -> List[State]:
    for individuo in populacao:
        rand = random.uniform(0, 1)
        if rand <= mutation_ratio:
            mutacao(individuo)

def elitismo(populacao : List[State]):
    melhor_sse = 999999999.0
    segundo_melhor_sse = 999999999.0
    elite : List[Grupo] = [None] * 2
    for individuo in populacao:
        if individuo.sse_total < melhor_sse:
            melhor_sse = individuo.sse_total
            elite[0] = individuo
        elif individuo.sse_total < segundo_melhor_sse:
                segundo_melhor_sse = individuo.sse_total
                elite[1] = individuo
    return elite

def valor_total_sse(populacao : List[State]):
    valor_total = 0.0
    for state in populacao:
        valor_total += state.sse_total
    return valor_total

def retorna_prob(individuo):
    return individuo[0]

def crossover_step(populacao : List[State], crossover_ratio : float) -> List[State]:
    nova_populacao = []
    roleta = construtor_roleta(populacao)
    for _ in range (round(len(populacao)/2)-1):
        selecao = np.random.choice(a=populacao, size=2, p=roleta, replace=False)
        if selecao is None:
            return populacao

        rand = random.uniform(0, 1)
        mae = selecao[0] 
        pai = selecao[1]

        if rand <= crossover_ratio:
            filho, filha = crossover(mae, pai)
        else:
            filha, filho = mae, pai

        nova_populacao = nova_populacao + [filho, filha]
    # # mantem elite
    nova_populacao = nova_populacao + [populacao[0], populacao[1]]
        
    return nova_populacao

def construtor_roleta(populacao : List[State]) -> List[tuple]:
    total_value = valor_total_sse(populacao) #soma os sse
    dists = [] #lista de todos os sse
    k = len(populacao)

    for state in populacao:
        dists.append(state.sse_total)
    
    prob = list(
        map (
            lambda x: (
                1 - (x/total_value))/(k-1), dists
        )
    )

    return prob

def rodar_roleta(roleta : List[tuple], rounds : int) -> List[State]:
    encontrou_estado = False
    start = time.process_time()
    end = 0

    if roleta == []:
        print("Roleta vazia, algo deu muito errado")
        exit(1)
    selecao = []
    while len(selecao) < rounds:
        prob = random.random()
        for _ in range(0, len(roleta)-1):
            random_index = random.randint(0, len(roleta)-1)
            state = roleta[random_index]
            if prob <= state[0]:
                encontrou_estado = True
                selecao.append(state[1])
                break
        if encontrou_estado:
            encontrou_estado = False
        else:
            for state in roleta:
                if prob <= state[0]:
                    selecao.append(state[1])
                    break
        if (time.process_time() - start) > 1:
            return None
    return selecao

def selecao(populacao : List[State], tam_populacao) -> List[State]:
    aux_populacao = construtor_roleta(populacao)
    nova_populacao = np.random.choice(a=populacao, size=tam_populacao, p=aux_populacao, replace=False)
    return nova_populacao

def print_populacao(populacao : List[State]):
    for state in populacao:
        print(round(state.sse_total, 4))

def genetic (k : int, pontos):
    max_time : int = 0.5
    tam_populacao, cross_ratio, mut_ratio = extrai_parametro((50.0, 0.75, 0.2))

    if type(tam_populacao) is float:
        tam_populacao = int(tam_populacao)
    opt_state : State = None
    opt_value = 9999999999999.0
    populacao : List[State]= gerar_populacao(k, pontos, tam_populacao)
    iter = 0    
    start = time.process_time()
    end = 0
    conv = False

    while not conv and end-start <= max_time:
        
        elite : List[State] = elitismo(populacao)

        best : State = elite[0]
        val_best : float = best.sse_total

        if val_best < opt_value:
            opt_state = best.deepcopy()
            opt_value = val_best

        selected = selecao(populacao, len(populacao)) 
        crossed = crossover_step(selected, cross_ratio)
        mutacao_step(crossed, mut_ratio)
        populacao = crossed
        conv = convergent(populacao)
        iter+=1
        end = time.process_time()
        
    lista = []
    for grupo in opt_state.grupos:
        lista.append(list(grupo.centroide))
    lista = np.array(lista, np.float32)
    return lista
