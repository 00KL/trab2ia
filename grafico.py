# ------------Libs importadas------------
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

# Essa classe tem por objetivo criar um gráfico que atenda 
# especificação:
# "Os resultados de cada classificador devem ser apresentados numa tabelacontendo 
# a média das acurácias obtidas em cada fold, o desvio padrão 
# e o intervalo deconfiança a 95% de significância dos resultados, 
# e também através do boxplot dos resultadosde cada classificador em cada fold"

class Grafico:
    def __init__(self,dataSet):
        self.tabela = []
        self.boxplot = []
        self.results = [self.tabela, self.boxplot]
        self.nome = "imagens/" +dataSet.capitalize()+".png"
        self.dataset = dataSet
        self.df = None

    def add(self,scores, classificador, mean, std, inf, sup):
        # o Grafico é formado por figuras que representam os valores 
        # contidos em s, ou seja cada método terá o conteudo de s 
        # representado no gráfico
        s = {
            'classifier': classificador,
            'mean': mean,
            'std':std,
            'inf':inf,
            'sup':sup
        }

        # adiciona a figura q irá representar certo clasificador
        # no gráfico, será o eixo X
        self.results[0].append(s)
        # adiciona o score referente ao classificador
        # será o eixo Y
        self.results[1].append(scores)
    
    def cm_to_inch(self,value):
        return value/2.54

    def showGrafico(self):
        # Classf será uma lista de classificadores
        classf = [r['classifier'] for r in self.results[0]] 
        # results[1] terá os scores de cada método
        df2 = pd.DataFrame(self.results[1], index = classf).T
        self.df = df2
        sns.boxplot(data=df2, showmeans=True)

        plt.yticks(np.arange(0, 1.1, step=0.1))
        plt.xlabel('Classificador')
        plt.ylabel('Acurácia')
        figure = plt.gcf()
        figure.set_size_inches(14, 6)
        plt.savefig(self.nome,orientation='landscape', dpi = 100)
        plt.close()

    def saveData(self):
        self.df.to_csv(r'scores/'+self.dataset+'.csv', index = False, header=True)
    