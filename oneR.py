# ------------Libs importadas------------
from typing import List
import numpy as np
import pandas as pd
import random
import re

class OneR:
    def __init__(self):
        self.melhor_coluna = 0
        self.proporcoes = None

    def predict(self, dataSet_X):
        # print(self.melhor_coluna)
        # print(self.proporcoes)
        # self.proporcoes.to_csv(r'test.csv')
        coord = dataSet_X[0:,self.melhor_coluna]
        # print(coord)

        lista = []

        for i in coord:
            # print("coordenada: ",i)
            test = self.proporcoes.loc[i].tolist()
            # print("Proporções: ",test)
            saida = self.roulette_run(test)
            columName = self.proporcoes.columns[saida]
            classe = re.findall("[0-9,.]+", columName)[0]
            # print("Resposta: ", classe)
            lista.append(int(classe))
            # print("\n")
            # exit()
        return lista
    
    def roulette_run (self, entrada):
        selected = []
        roulette = entrada.copy()
        roulette.sort()
        r = random.uniform(0,roulette[-1])
        # print("R",r)
        for state in roulette:
            # print("State: ",state)
            if r <= state:
                # print("entrou")
                # print("Index entrada: ", entrada.index(state))
                # print("Index roulette: ", roulette.index(state))
                return entrada.index(state)
        return selected

    def fit(self,dataSet_X, dataSet_Y):
        caracteristica, melhor_coluna = self.melhor_caracteristica(dataSet_X, dataSet_Y)
        # print("Tabela de Contigênciamento")
        # print(caracteristica)

        pd_aux = caracteristica.sum(axis=1)
        # print("\nSoma de frequencais por linha")
        # print(pd_aux)

        aux = caracteristica.div(pd_aux, axis='rows')
        # print("\nFrequencia/soma_frequencia")
        # print(aux)

        self.melhor_coluna = melhor_coluna
        self.proporcoes = aux
        # print(self.melhor_coluna)
        # print(self.proporcoes)


    def melhor_caracteristica(self,dataSet_X, dataSet_Y):
        # Define lista com as classes de um Dataset
        classes = np.unique(dataSet_Y)
        
        # Tamanho da linha da tabela de contigência
        # Valor da coordenada | Classe 1 | ... | Classe n 
        tam_line = classes.size

        # Cada coluna do dataSet_X se torna uma tabela
        quant_tables = dataSet_X[0].size

        # Melhor característica 
        maior_pontuacao = 0
        melhor_coluna = 0
        melhor_caracteristica = None

        for i in range(0, quant_tables):
            caracteristica = self.tabelaContigencia(dataSet_X, dataSet_Y, i, tam_line, classes)
            pd_aux = caracteristica.max(axis=1)
            pontuacao = pd_aux.sum(axis=0)
            if maior_pontuacao < pontuacao:
                maior_pontuacao = pontuacao
                melhor_coluna = i
                melhor_caracteristica = caracteristica

        # print("A coluna ",melhor_coluna," é a melhor coluna de referencia.")
        return melhor_caracteristica, melhor_coluna


    def tabelaContigencia(self,dataSet_X, dataSet_Y, coluna, tam_line, classes):
        # Lista de valores únicos da primeira coluna 
        # do dataset 
        coord_unic = np.unique(dataSet_X[0:,coluna])
        
        # linhas da tabela
        linhas = []

        linha = ['classe'] * tam_line
        coordenada = 'coordenada ' + str(0)
        linha[0] = coordenada
        aux = 0
        for c in classes:
            linha[aux] = 'classe ' + str(c)
            aux+=1

        linhas.append(linha)
        for unic in coord_unic:
            line = [0] * tam_line
            for index in range(0,len(dataSet_X)):
                # print(dataSet_X[index][0])
                # print(unic)
                # print(dataSet_X[index][0] == unic)
                if dataSet_X[index][coluna] == unic:
                    indexAxu = self.npArrayPrimeiroIndex(classes, dataSet_Y[index])
                    # print(type(indexAxu))
                    line[indexAxu] += 1
            # print(line)
            linhas.append(line)
        # print(linhas[0])
        tabela = pd.DataFrame( linhas[1:],columns=linhas[0], index=coord_unic)
        # print(tabela)
        return tabela

    # retorna o index da primeira ocorrencia de um dado elemento
    # em uma npArray
    def npArrayPrimeiroIndex(self, npArray, elemento):
        return  np.where(npArray == elemento)[0][0]