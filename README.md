# Segundo Trabalho de Inteligência Artificial. 
#### Em primeiro lugar é preciso estar como ambiente Python configurado. Para isso faça:

    python -m venv env
    . /env/bin/activate
    pip install -r requirements.

#### Para executar o programa em python faça:

    python main.py

#### Nessa execução é preciso que as pastas imagens, scores, tabelas, tabela_pareada e util estejam presentes no diretório, além dos arquivos genetic.py, grafico.py, kgacentroides.py, kmeanscentroides.py. Serão criados 2 arquivos tex (arquivos em liguagem latex) -> tabela de pareamento e de resultados das duas primeiras etapas 1 csv -> scores coletados nos métodos 2 imagens(png) -> gráficos da tebela das duas primeiras etapas e uma images contendo a tabela de paramento.

#### Para executar o programa em um python notebook faça:

    jupyter notebook

#### Uma interface web irá se abrir no navegador padrão, nele deve-se clicar em main2.ipynb e, ao abrir o arquivo clique no botão de play que estará na barra de menus acima do código. Serão criados 2 arquivos tex (arquivos em liguagem latex) -> tabela de pareamento e de resultados das duas primeiras etapas, 1 csv -> scores coletados nos métodos, 2 imagens(png) -> gráficos da tebela das duas primeiras etapas e uma images contendo a tabela de paramento. Diferente do primeiro esses arquivos não estarão em pastas separadas, todos serão criados na mesma pasta do main2.ipynb.

#### A execução de todo o programa demora em torno de 2h20min~2h:40min.

