from typing import List
import seaborn as sns
import matplotlib.pyplot as plt

def extrai_parametro(tupla : tuple):
    return tupla[0], tupla[1], tupla[2]

def remove_repetidos(lista):
    l = []
    for i in lista:
        if i not in l:
            l.append(i)
    return l

def rankeamento(media_sse : List[float]) -> List[int]:
    media_copy = media_sse.copy()
    media_copy.sort(reverse = False)
    ranking = []
    for media in media_sse:
        for j in range(0,len(media_copy)):
            if media == media_copy[j]:
                ranking.append(j) 
                break
    return ranking

def simple_table(eixox : List, eixoy : List):
    sns.boxplot(y=eixoy, x=eixox) # Also accepts numpy arrays
    plt.show()

def dataset_table(dataset):
    # Make boxplot for each group
    sns.boxplot( data=dataset.loc[:2,:] )
    # loc[:,:] means all lines and all columns
    plt.show()