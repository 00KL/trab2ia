from .grupo import Grupo
from typing import List
import random

# TODO alterar init 
# deepcopy personalizado

class State:
    def __init__(self, k : int, points : List[List [float]]):
        self.grupos : Grupo = []
        self.sse_total = 0.0
        self.points = points
        self.k = k
    
    # TODO perturbar menos estados
    # mover grupo q tem a maior sse
    #essa função deve pegar o ponto mais distante de cada grupo 
    #e enviar ele para um grupo onde ele fique mais proximo do centroid
    def pertubarcao_estado_random(self):       
        perde_ponto : Grupo = self.random_grupo()
        ponto : tuple = perde_ponto.remove_random_ponto()
        ganha_ponto : Grupo = self.random_grupo()
        ganha_ponto.add_ponto(ponto)
        self.calc_sse_total()

    def pertubarcao_estado(self):
        # ganha_ponto : Grupo = None
        # grupo_perde_ponto : Grupo = None
        perde_ponto = self.grupo_mais_distante()
        if perde_ponto is None:
            return
        # print("test1")
        ponto, dist  = perde_ponto.ponto_mais_distante()
        # print("test2")
        ganha_ponto : Grupo = self.random_grupo()
        # print("test3")
        ganha_ponto.add_ponto(ponto)
        # print("test4")
        self.calc_sse_total()

    def limpa_grupos(self):
        for grupo in self.grupos:
            grupo.limpa_grupo()

    def recalc_grupos(self):
        for grupo in self.grupos:
            grupo.calc_centroid()
            grupo.calc_sse()

    def min_local(self, pontos):
        dist_menor = 99999
        dist_atual = 0.0
        melhor_grupo : Grupo = None
        grupo : Grupo = None
        self.limpa_grupos()

        for ponto in pontos:
            for grupo in self.grupos:
                dist_atual = grupo.calc_dist_centroid(ponto)
                if dist_menor > dist_atual:
                    melhor_grupo = grupo
                    dist_menor = dist_atual
            melhor_grupo.add_ponto_guloso(ponto)
            dist_menor = 999999
        
        self.recalc_grupos()
        self.calc_sse_total()

    def init_state(self):
        i = 0
        for i in range(0, self.k):
            self.grupos.append(Grupo())
            self.grupos[i].pontos.append(tuple(self.points[i]))
        
        for i in range(self.k, len(self.points)):
            index_grupo = random.randint(0,self.k-1)
            self.grupos[index_grupo].pontos.append(tuple(self.points[i]))

        for grupo in self.grupos:
            grupo.calc_centroid()
            grupo.calc_sse()
    
    def calc_sse_total(self):
        self.sse_total = 0.0
        for grupo in self.grupos:
            self.sse_total += grupo.sse

    def random_grupo(self):
        index = 0
        index2 = 0
        for _ in range(0, len(self.grupos)-1):
            index = random.randint(0,len(self.grupos)-1)
            if len(self.grupos[index].pontos) > 2:
                break
        for _ in range(0, len(self.grupos)-1):
            index2 = random.randint(0,len(self.grupos)-1)
            if len(self.grupos[index2].pontos) > 2:
                break
        if self.grupos[index].sse < self.grupos[index2].sse:
            return self.grupos[index2]
        return self.grupos[index]
        
    def deepcopy(self):
        new_state = State(self.k, self.points)
        new_state.sse_total = self.sse_total
        for grupo in self.grupos:
            new_state.grupos.append(grupo.copy_grupo()) 
        return new_state

    def grupo_mais_distante(self):
        maior_sse = 0.0
        grupo_mais_distante = None
        for grupo in self.grupos:
            if maior_sse < grupo.sse and len(grupo.pontos) > 2:
                maior_sse = grupo.sse
                grupo_mais_distante = grupo
        return grupo_mais_distante

    def igualdade(self, state):
        grupo : Grupo = None
        grupo2 : Grupo = None
        for grupo in self.grupos:
            for grupo2 in state.grupos:
                if grupo.centroide != grupo2.centroide:
                    return False
        return True

    def compara_sse(self, state):
        return self.sse_total == state.sse_total
        