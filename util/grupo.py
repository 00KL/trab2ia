from typing import List
import numpy as np
import random

class Grupo:
    def __init__(self):
        self.sse = 0.0
        self.centroide = ()
        self.pontos= []
        self.dist= []
    
    def __str__(self):
        # print("\nSSE: ", self.sse)
        # print("\nCentroide: ", self.centroide)
        # print(self.pontos)
        return ""
    
    def limpa_grupo(self):
        self.pontos = []

    def calc_centroid(self):
        self.centroide = ()
        # print(self.pontos)
        # pontos é composto de diversas tuplas, zip irá juntar todas as tuplas de cada posição
        # ex: todas as tuplas da posição '0' serão organizadas em uma tupla
        # o caracater '*' é necessário para que a função zip consiga extrair so componente do vetor de tuplas
        pontos_zipados = tuple(zip(*self.pontos))
        # self.centroide = tuple(sum(x)/len(self.pontos) for x in pontos_zipados)
        # cada x será uma das tuplas do retorno de zip
        # print(self.pontos)
        for x in pontos_zipados:
        #     # soma todos os valores da tupla x e dividi-se os mesmos pelo número de pontos
        #     # adiciona o resultado da soma na tupla centroide daquele grupo
            # print("\nx = ", x)
            self.centroide = self.centroide + (sum(x)/len(self.pontos),)
        # print("---------centroid----------", len(self.centroide), self.centroide)
        

    def calc_dist_centroid(self,ponto):
        # print(self.centroide)
        # print(ponto)
        ponto_array = np.array(ponto)
        dist = np.linalg.norm(ponto_array - self.centroide)
        return dist

    def calc_sse(self):
        self.sse = 0.0
        self.dist = [None]*len(self.pontos)
        # print("Tamanho do grupo: ",len(self.pontos))
        i = 0
        for ponto in self.pontos:
            dist = self.calc_dist_centroid(ponto)
            # print("Tamanho do grupo: ",len(self.pontos)," I: " , i )
            self.dist[i] = dist
            # print("ok")
            i += 1
            self.sse += dist**2

    def ponto_mais_distante(self):
        # print("testando-------------")
        maior_distancia = 0
        ponto_mais_distante = None
        # print(self.pontos)
        # print("Centroid:", self.centroide)
        i = 0
        for ponto in self.pontos:
            # print("Tamanho do grupo: ",len(self.pontos)," I: " , i )
            # print("Index do ponto atual: ", self.pontos.index(ponto))
            distancia_atual = self.dist[i]
            # print("ok")
            i+=1
            # print("Distância atual: ",distancia_atual)
            if maior_distancia < distancia_atual:
                maior_distancia = distancia_atual
                # print("Maior distância atual: ", maior_distancia)
                ponto_mais_distante = ponto
                # print("Ponto mais distante: ", ponto_mais_distante)
        if ponto_mais_distante is None:
            # print(self.pontos)
            # print(self.sse)
            # print(self.centroide)
            return None, None
        # print(self.ponto_distante == ponto_mais_distante)
        self.pontos.remove(ponto_mais_distante)
        self.calc_centroid()
        self.calc_sse()

        # if(ponto_mais_distante == self.ponto_mais_distante)

        # print(maior_distancia)
        # print(self.pontos)
        # print("\n\n\n")
        return ponto_mais_distante, maior_distancia

    def remove_random_ponto(self) -> tuple:
        # print(len(self.pontos)-1)
        index = random.randint(0,len(self.pontos)-1)
        ponto = self.pontos.pop(index)
        self.calc_centroid()
        self.calc_sse()
        return ponto

    def add_ponto(self, ponto : tuple):
        # print("Add ponto")
        # print(len(self.pontos))
        self.pontos.append(ponto)
        # print(len(self.pontos))
        self.calc_centroid()
        self.calc_sse()
        # if len(self.pontos) != len(self.dist): 
        #     print("Numero de pontos diferentes de número de distancias: Pontos=",len(self.pontos), "Distancias=", len(self.dist) )
        #     exit(1)

    def add_ponto_guloso(self, ponto):
        self.pontos.append(ponto)
    
    def copy_grupo(self):
        # print("Copy grupo")
        copy = Grupo() 
        copy.pontos = self.pontos.copy()
        copy.sse = self.sse
        copy.centroide = self.centroide
        copy.dist = self.dist.copy()
        # if len(copy.pontos) != len(copy.dist): 
        #     print("Numero de pontos diferentes de número de distancias: Pontos=",len(copy.pontos), "Distancias=", len(copy.dist) )
        #     exit(1)
        return copy