# ------------Libs importadas------------
import numpy as np
import pandas as pd
import time
import warnings
import math

# ------------sklearn------------
from sklearn import datasets
from sklearn.dummy import DummyClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn import preprocessing
from sklearn.model_selection import RepeatedStratifiedKFold
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import cross_val_score
from scipy import stats
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.base import BaseEstimator
from scipy.stats import ttest_rel, wilcoxon
import imgkit

# ------------Classes implemantadas------------
from oneR import OneR
from kmeanscentroides import KCentroides
from kgacentroides import KCentroides as KGA
from grafico import Grafico

# Treino e teste com 3 rodadas de validação cruzadaestratificada de 
# 10 folds dos classificadores que não possuem hiperparâmetros
def classificacao(dataSet_X, dataSet_Y, classificador, isOneR=None, grade=None):
    
    scalar = StandardScaler()
    gs = None
    
    if isOneR is None:
        pipeline = Pipeline([('transformer', scalar), ('estimator', classificador)])
        if grade is not None:
            gs = GridSearchCV(estimator = pipeline, param_grid = grade, scoring='accuracy', cv = 4 )
    else:
        classes = np.unique(dataSet_Y)
        norm = preprocessing.KBinsDiscretizer(n_bins=classes.size*2, encode='ordinal', strategy='kmeans')
        
        pipeline = Pipeline([('transformer', scalar), ('normalizador', norm), ('estimator', classificador)])

    # ------------Permutação do Dataset------------
    rkf = RepeatedStratifiedKFold(n_splits=10, n_repeats=3)
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        if gs is None:
            scores = cross_val_score(pipeline, dataSet_X, dataSet_Y, scoring='accuracy', cv = rkf)
        else:
           scores = cross_val_score(gs, dataSet_X, dataSet_Y, scoring='accuracy', cv = rkf) 

    mean = scores.mean()
    std = scores.std()
    inf, sup = stats.norm.interval(0.95, loc=mean,scale=std/np.sqrt(len(scores)))

    # print("\nMean Accuracy: %0.2f Standard Deviation: %0.2f" % (mean, std))
    # print ("Accuracy Confidence Interval (95%%): (%0.2f, %0.2f)\n" % (inf, sup))
    return scores, mean, std, inf, sup

# Treino e teste com 3 rodadas de validação cruzadaestratificada de 
# 10 folds dos classificadores que não possuem hiperparâmetros, isto 
# é, os classificadores ZeroR, Aleatório, Aleatório Estratificado, OneR 
# Probabilístico e Naive BayesGaussian para um dado DataSet
def calc(dataSet_X, dataSet_Y, dataSet):
    grafico : Grafico = Grafico(dataSet)
    colunas = ['Método', 'Média', 'Desvio Padrão', 'Limite Inferior', 'Limite Superior']
    linhas = []
    start = time.process_time()

    print("\n------------KNeighborsClassifier------------")
    oneNN = KNeighborsClassifier(n_neighbors=1)
    classificacao(dataSet_X, dataSet_Y, oneNN, False)
    

    # Os classificadores  ZeroR, Aleatório e Aleatório 
    # Estratificado correspondem ao uso do DummyClassifier   
    # estabelecendo   respectivamente   o   valor   do   
    # parâmetro   strategy   comomost_frequent, uniform e 
    # stratified.

    # ZeroR = DummyClassifier(strategy="most_frequent")
    # Aleatório = DummyClassifier(strategy="uniform")
    # Aleatório Estratificado = DummyClassifier(strategy="stratified")

    print("\n------------ZeroR------------")
    zeroR = DummyClassifier(strategy="most_frequent")
    score, mean, std, inf, sup = classificacao(dataSet_X, dataSet_Y, zeroR)
    linhas.append(['ZeroR', mean, std, inf, sup])
    grafico.add(score, 'ZeroR', mean, std, inf, sup)

    print("\n------------Aleatório------------")
    aleatorio = DummyClassifier(strategy="uniform")
    score, mean, std, inf, sup = classificacao(dataSet_X, dataSet_Y, aleatorio)
    linhas.append(['Aleatório', mean, std, inf, sup])
    grafico.add(score, 'Aleatório', mean, std, inf, sup)

    print("\n------------Aleatório Estratificado------------")
    aleatorioEstratificado = DummyClassifier(strategy="stratified")
    score, mean, std, inf, sup = classificacao(dataSet_X, dataSet_Y, aleatorioEstratificado) 
    linhas.append(['Aleatório E', mean, std, inf, sup])
    grafico.add(score, 'Aleatório E', mean, std, inf, sup)

    print("\n------------OneR------------")
    oner = OneR()
    score, mean, std, inf, sup = classificacao(dataSet_X, dataSet_Y, oner, isOneR=True)
    linhas.append(['OneR', mean, std, inf, sup])
    grafico.add(score, 'OneR', mean, std, inf, sup)

    print("\n------------Gaussian Naive Bayes------------")
    gaussianNB = GaussianNB()
    score, mean, std, inf, sup = classificacao(dataSet_X, dataSet_Y, gaussianNB)
    linhas.append(['GaussianNB', mean, std, inf, sup])
    grafico.add(score, 'GaussianNB', mean, std, inf, sup)

    print("\n------------KmeansCentroides-----------")
    kc = KCentroides()
    grade={'estimator__k': [1, 3, 5, 7]}
    score, mean, std, inf, sup = classificacao(dataSet_X, dataSet_Y, kc, None, grade)
    linhas.append(['KMeans', mean, std, inf, sup])
    grafico.add(score, 'KMeans', mean, std, inf, sup)

    print("\n------------KGACentroides-----------")
    kga = KGA()
    grade={'estimator__k': [1, 3, 5, 7]}
    score, mean, std, inf, sup = classificacao(dataSet_X, dataSet_Y, kga, None, grade)
    linhas.append(['KGA', mean, std, inf, sup])
    grafico.add(score, 'KGA', mean, std, inf, sup)

    print("\n------------KNeighborsClassifier-----------")
    KNN = KNeighborsClassifier()
    grade={'estimator__n_neighbors': [1, 3, 5, 7]}
    score, mean, std, inf, sup = classificacao(dataSet_X, dataSet_Y, KNN, None, grade)
    linhas.append(['KNN', mean, std, inf, sup])
    grafico.add(score, 'KNeighbors', mean, std, inf, sup)

    print("\n------------KNeighborsClassifier DISTANCE------------")
    DistKNN = KNeighborsClassifier(weights = 'distance')
    grade={'estimator__n_neighbors': [1, 3, 5, 7]}
    score, mean, std, inf, sup = classificacao(dataSet_X, dataSet_Y, DistKNN, None, grade)
    linhas.append(['DistKNN', mean, std, inf, sup])
    grafico.add(score, 'DistKNN', mean, std, inf, sup)


    print("\n------------Árvores de Decisão------------")
    dt = DecisionTreeClassifier()
    grade={'estimator__max_depth': [None, 3, 5, 10]}
    score, mean, std, inf, sup = classificacao(dataSet_X, dataSet_Y, dt, None, grade)
    linhas.append(['DTree', mean, std, inf, sup])
    grafico.add(score, 'DTree', mean, std, inf, sup)

    print("\n------------Florestas de Árvores Aleatórias------------")
    rF = RandomForestClassifier()
    grade={'estimator__n_estimators': [10, 20, 50, 100]}
    score, mean, std, inf, sup = classificacao(dataSet_X, dataSet_Y, rF, None, grade)
    linhas.append(['RForest', mean, std, inf, sup])
    grafico.add(score, 'RForest', mean, std, inf, sup)

    path = "./tabelas/" + dataSet + ".tex"

    tabela = pd.DataFrame(linhas, columns=colunas)

    grafico.showGrafico()
    grafico.saveData()
    
    tabela.to_latex(
        label=dataSet,
        buf=path,
        na_rep="",
        float_format="%.4f",
        escape=False,
        caption=dataSet.capitalize(),
        index=False
    )

def significancia(val):
    if type(val) == str:
        return 
    bold = '' if val <= 0.05 else ''
    return 'font-weight: %s' % bold
    
styles = [
        #table properties
        dict(selector="", 
             props=[("margin","0"),
                    ("font-family",'"Helvetica", "Arial", sans-serif'),
                    ("border-collapse", "collapse"),
                    ("border-spacing","0"),]),

        #background shading
        dict(selector="tbody tr:nth-child(even)",
             props=[("background-color", "#fff")]),
        dict(selector="tbody tr:nth-child(odd)",
             props=[("background-color", "#eee")]),

        #cell spacing
        dict(selector="td", 
             props=[("padding", ".5em"),
                    ("font-size","96%")]),

        #header cell properties
        dict(selector="th", 
             props=[("text-align", "center"),
                    ("visibility", "collapse")]),

    ]

def significancia(val):
    try:
        val = float(val)
    except:
        return ''
    
    bold = 'bold' if val <= 0.05 else ''
    return 'font-weight: %s' % bold

def to_str(val):
    if(type(val) ==  type('string')):
        return val
    return "%.2g" % val
    

def pareamento(dataSet):
    df = pd.read_csv('scores/'+dataSet+'.csv') 
    entrada = df.to_dict('list')
    metodos = df.columns.tolist()
    t_par = []
    linha = []
    coluna = []
    wilcox = []
    aux = 0

    for m1 in entrada:
        metodo = metodos[aux]
        aux += 1
        m1 = entrada[m1]
        for i in range(aux-1, len(metodos)):
            m2 = metodos[i]
            m2 = entrada[m2]
            s,p = ttest_rel(m1,m2)
            if math.isnan(p):
                linha.append(metodo)
            else:
                linha.append(p)
        t_par.append(linha)
        linha = []
    aux = 0

    for i in range(len(metodos)-1, -1, -1):
        metodo = metodos[i]
        m1 = entrada[metodo]
        for j in range(0, len(metodos)-aux):
            m2 = metodos[j]
            m2 = entrada[m2]
            try:
                s,p = wilcoxon(m1,m2)
                coluna.append(p)
            except ValueError as val_e:
                coluna.append(metodo)
        coluna = coluna + t_par[i][1:]
        wilcox.append(coluna)
        coluna = []
        aux += 1
    pd.set_option('display.float_format', '{:.2g}'.format)
    df = pd.DataFrame(wilcox)
    df = df.iloc[::-1]
    df = df.applymap(to_str)

    s = df.style.applymap(significancia).set_table_styles(styles).hide_index()
    html = s.render()
    imgkitoptions = {"format": "png"}
    imgkit.from_string(html, "./tabela_pareada/"+dataSet+".png")

    path = "./tabela_pareada/" + dataSet + ".tex"
    df.to_latex(
        label=dataSet,
        buf=path,
        bold_rows=True,
        caption="Pareamento "+dataSet.capitalize(),
        index=False,
        header=False
    )

def selectDataSet(dataSet):
    if dataSet == 'iris':
        ds = datasets.load_iris()
    elif dataSet == 'wine':
        ds = datasets.load_wine()
    elif dataSet == 'digits':
        ds = datasets.load_digits()
    elif dataSet == 'breast_cancer' or dataSet == 'cancer':
        ds = datasets.load_breast_cancer()
    else:
        print("DataSet não disponível.")
        print("Tente: iris, wine, digits, breast_cancer ou cancer.")
        exit()
    ds_x = ds.data
    ds_y = ds.target
    calc(ds_x, ds_y, dataSet)
    pareamento(dataSet)

df = selectDataSet('iris')
# df = selectDataSet('wine')
# df = selectDataSet('digits')
# df = selectDataSet('cancer')
    
    
