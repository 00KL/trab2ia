# ------------Libs importadas------------
from typing import List
import numpy as np
import pandas as pd
import random
import re
from sklearn import cluster
from sklearn.base import BaseEstimator

class KCentroides (BaseEstimator):
    def __init__(self, k = 1):
        super().__init__()
        self.centroides_classe = []
        self.k = k

    def predict(self, dataSet_X):
        lista = []

        # Percorre pontos no datase_X
        for ponto in dataSet_X:
            # Inicia uma distância enorme que será 
            # maior que qualquer outra alcançada 
            # na execução do código
            menor_dist = 999999999999999999999
            # Como cada grupo dentro dos centroides_classe
            # representa uma classe, e as classes são valores
            # inteiroas, iremos aqui percorrer todos as classes
            for classe in range(0, (len(self.centroides_classe))):
                centroides = self.centroides_classe[classe]
                # Percorrer centroides de cada grupo
                for centroide in centroides:
                    dist = self.calc_dist_centroid(centroide,ponto)
                    if(dist < menor_dist):
                        menor_dist = dist
                        classe_escolhida = classe
            lista.append(classe_escolhida)
        return lista

    def fit(self,dataSet_X, dataSet_Y):
        aux = np.arange(0, dataSet_X[0].size, 1).tolist()
        colunas = []
        for c in aux:
            colunas.append('coord ' + str(c))
            
        tabela = pd.DataFrame( dataSet_X, columns=colunas)
        tabela['classe'] = dataSet_Y
        grupos_por_classe = tabela.groupby(tabela.classe)

        classes = np.array(dataSet_Y)
        for c in np.unique(classes):
            lista = grupos_por_classe.get_group(c).iloc[:,:-1].values.tolist()
            kminhos = cluster.KMeans(n_clusters=self.k, random_state=0).fit(lista)
            centroides = kminhos.cluster_centers_

            self.centroides_classe.append(centroides)

    def calc_dist_centroid(self,centroide,ponto):
            ponto_array = np.array(ponto)
            dist = np.linalg.norm(ponto_array - centroide)
            return dist